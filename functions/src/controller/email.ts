import * as util from "../util";
import * as service from "../service";
import * as admin from "firebase-admin";

export default (firebaseAdmin: admin.app.App) => {
  const emailController = util.express();
  emailController.use(service.user.verifyTokenService(firebaseAdmin));
  emailController.get(
    "/",
    service.email.getHistoryByMemberIdService(firebaseAdmin)
  );
  emailController.post("/send", service.email.sendEmailService(firebaseAdmin));
  emailController.get(
    "/provider",
    service.email.providerDiscoveryService(firebaseAdmin)
  );
  emailController.put(
    "/provider",
    service.email.providerUpdateService(firebaseAdmin)
  );
  emailController.post(
    "/provider/register",
    service.email.providerRegisterService(firebaseAdmin)
  );
  emailController.use("*", service.healthcheck.healthCheckService);
  return emailController;
};
