import * as util from "../util";
import * as service from "../service";
import * as admin from "firebase-admin";

export default (firebaseAdmin: admin.app.App) => {
  const userController = util.express();
  userController.post("/signup", service.user.signupService(firebaseAdmin));
  userController.use("*", service.healthcheck.healthCheckService);
  return userController;
};
