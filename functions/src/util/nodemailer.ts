import * as nodemailer from "nodemailer"
import * as admin from "firebase-admin"
import * as express from "express"
import * as repository from "../repository"
import { IRequest } from "../model/dto/express"

const transport = nodemailer.createTransport({
  host: "smtp.siteoffs.com",
  port: 587,
  secure: false,
  auth: {
    user: "no-reply@siteoffs.com",
    pass: "mRWgOqcuW",
  },
  tls: {
    rejectUnauthorized: false,
  },
})

export default async (
  req: IRequest,
  res: express.Response,
  firebaseAdmin: admin.app.App
) => {
  const { recipients, subject, message } = req.body
  await transport.sendMail({
    from: "Fastwork EmailService <no-reply@siteoffs.com>",
    to: recipients,
    subject,
    text: message,
  })
  const writeRef = await repository.history.createHistory(firebaseAdmin, {
    ...req.body,
    recipients: recipients,
    message: message,
    from: "no-reply@siteoffs.com",
    provider: "nodemailer",
    status: "SUCCESS",
    consumerId: req.userInfo?.uid,
  })
  const readSnapshot = await writeRef.get()
  return res.status(200).send({
    data: {
      id: writeRef.id,
      ...readSnapshot.data(),
      createdAt: readSnapshot.data()?.createdAt._seconds * 1000,
    },
  })
}
