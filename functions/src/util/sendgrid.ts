import * as sgMail from "@sendgrid/mail"
import * as admin from "firebase-admin"
import * as express from "express"
import * as repository from "../repository"
import { IRequest } from "../model/dto/express"

sgMail.setApiKey(
  "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"
)

export default async (
  req: IRequest,
  res: express.Response,
  firebaseAdmin: admin.app.App
) => {
  const { recipients, subject, message } = req.body
  await sgMail.send({
    from: "Fastwork EmailService <no-reply@siteoffs.com>",
    to: recipients,
    subject,
    text: message,
  })
  const writeRef = await repository.history.createHistory(firebaseAdmin, {
    ...req.body,
    recipients: recipients,
    message: message,
    from: "no-reply@siteoffs.com",
    provider: "sendgrid",
    status: "SUCCESS",
    consumerId: req.userInfo?.uid,
  })
  const readSnapshot = await writeRef.get()
  return res.status(200).send({
    data: {
      id: writeRef.id,
      ...readSnapshot.data(),
      createdAt: readSnapshot.data()?.createdAt._seconds * 1000,
    },
  })
}
