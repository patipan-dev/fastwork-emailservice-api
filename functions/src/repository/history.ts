import * as admin from "firebase-admin";

export const getHistoryByMemberId = async (
  firebaseAdmin: admin.app.App,
  data: any
) => {
  return await firebaseAdmin
    .firestore()
    .collection("histories")
    .where("consumerId", "==", data.id)
    .orderBy("createdAt", "desc")
    .offset(parseInt(data.offset, 10) || 0)
    .limit(parseInt(data.limit, 10) || 20)
    .get();
};

export const createHistory = async (
  firebaseAdmin: admin.app.App,
  data: any
) => {
  return await firebaseAdmin
    .firestore()
    .collection("histories")
    .add({
      ...data,
      createdAt: admin.firestore.FieldValue.serverTimestamp()
    });
};
