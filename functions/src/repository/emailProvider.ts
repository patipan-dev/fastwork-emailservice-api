import * as admin from "firebase-admin";

export const getEmailProvider = async (firebaseAdmin: admin.app.App) => {
  return await firebaseAdmin
    .firestore()
    .collection("email_providers")
    .get();
};

export const getEmailProviderByName = async (
  firebaseAdmin: admin.app.App,
  data: any
) => {
  return await firebaseAdmin
    .firestore()
    .collection("email_providers")
    .where("providerName", "==", data)
    .get();
};

export const createEmailProvider = async (
  firebaseAdmin: admin.app.App,
  data: any
) => {
  return await firebaseAdmin
    .firestore()
    .collection("email_providers")
    .add({
      ...data,
      createdAt: admin.firestore.FieldValue.serverTimestamp()
    });
};

export const updateEmailProvider = async (
  firebaseAdmin: admin.app.App,
  data: any
) => {
  const { id, ...updateData } = data;
  return await firebaseAdmin
    .firestore()
    .collection("email_providers")
    .doc(id)
    .update(updateData);
};
