import { Request } from "express";
import * as admin from "firebase-admin";

export interface IRequest extends Request {
  userInfo?: admin.auth.DecodedIdToken;
}
