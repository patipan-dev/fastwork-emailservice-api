import * as express from "express";

export const healthCheckService = (
  _req: express.Request,
  res: express.Response
) => {
  return res.status(200).send("OK");
};
