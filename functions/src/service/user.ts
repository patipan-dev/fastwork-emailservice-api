import * as express from "express";
import * as admin from "firebase-admin";
import { IRequest } from "../model/dto/express";

export const verifyTokenService = (firebaseAdmin: admin.app.App) => async (
  req: IRequest,
  res: express.Response,
  next: express.NextFunction
) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    try {
      const userInfo = await admin
        .auth()
        .verifyIdToken(req.headers.authorization.split(" ")[1]);
      req.userInfo = userInfo;
      return next();
    } catch (error) {
      return res.status(401).send({
        code: "user/user-authorization-error",
        message: "You are not authorized to make this request."
      });
    }
  } else {
    return res.status(401).send({
      code: "user/user-authorization-error",
      message: "Cannot find authorization token."
    });
  }
};

export const signupService = (firebaseAdmin: admin.app.App) => async (
  req: IRequest,
  res: express.Response
) => {
  try {
    const user = await firebaseAdmin.auth().createUser(req.body);
    return res.status(200).send({ data: user });
  } catch (error) {
    return res.status(400).send(error);
  }
};
