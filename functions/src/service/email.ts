import * as express from "express"
import * as admin from "firebase-admin"
import * as util from "../util"
import * as repository from "../repository"
import { IRequest } from "../model/dto/express"

export const getHistoryByMemberIdService = (firebaseAdmin: admin.app.App) => async (
  req: IRequest,
  res: express.Response
) => {
  const { offset, limit } = req.query
  try {
    const readSnapshot = await repository.history.getHistoryByMemberId(
      firebaseAdmin,
      {
        id: req.userInfo?.uid,
        offset,
        limit,
      }
    )
    const data = readSnapshot.docs.map((historyData) => ({
      id: historyData.id,
      ...historyData.data(),
      createdAt: historyData.data().createdAt._seconds * 1000,
    }))
    return res.status(200).send({ data })
  } catch (error) {
    return res.status(400).send(error)
  }
}

export const sendEmailService = (firebaseAdmin: admin.app.App) => async (
  req: IRequest,
  res: express.Response
) => {
  const sendgridProviderSnapshot = await repository.emailProvider.getEmailProviderByName(
    firebaseAdmin,
    "sendgrid"
  )
  const nodemailerSnapshot = await repository.emailProvider.getEmailProviderByName(
    firebaseAdmin,
    "nodemailer"
  )

  if (
    !sendgridProviderSnapshot.empty &&
    sendgridProviderSnapshot.docs[0].data().enable
  ) {
    try {
      return await util.sendgrid(req, res, firebaseAdmin)
    } catch (error) {
      const writeRef = await repository.history.createHistory(firebaseAdmin, {
        ...req.body,
        recipients: req.body.to,
        message: req.body.text,
        from: "no-reply@siteoffs.com",
        provider: "sendgrid",
        status: "FAILED",
        consumerId: req.userInfo?.uid,
      })
      const readSnapshot = await writeRef.get()
      return res.status(400).send({
        code: "email/email-provider-error",
        message: "Something went wrong with sendgrid.",
        data: {
          id: writeRef.id,
          ...readSnapshot.data(),
          createdAt: readSnapshot.data()?.createdAt._seconds * 1000,
        },
      })
    }
  }

  if (!nodemailerSnapshot.empty && nodemailerSnapshot.docs[0].data().enable) {
    try {
      return await util.nodemailer(req, res, firebaseAdmin)
    } catch (error) {
      const writeRef = await repository.history.createHistory(firebaseAdmin, {
        ...req.body,
        recipients: req.body.to,
        message: req.body.text,
        from: "no-reply@siteoffs.com",
        provider: "nodemailer",
        status: "FAILED",
        consumerId: req.userInfo?.uid,
      })
      const readSnapshot = await writeRef.get()
      return res.status(400).send({
        code: "email/email-provider-error",
        message: "Something went wrong with email nodemailer.",
        data: {
          id: writeRef.id,
          ...readSnapshot.data(),
          createdAt: readSnapshot.data()?.createdAt._seconds * 1000,
        },
      })
    }
  }

  return res.status(500).send({
    code: "email/email-provider-error",
    message: "Something went wrong with email provider.",
  })
}

export const providerRegisterService = (firebaseAdmin: admin.app.App) => async (
  req: express.Request,
  res: express.Response
) => {
  try {
    const writeRef = await repository.emailProvider.createEmailProvider(
      firebaseAdmin,
      req.body
    )
    const readSnapshot = await writeRef.get()
    return res
      .status(200)
      .send({ data: { id: writeRef.id, ...readSnapshot.data() } })
  } catch (error) {
    return res.status(400).send(error)
  }
}

export const providerDiscoveryService = (firebaseAdmin: admin.app.App) => async (
  _req: express.Request,
  res: express.Response
) => {
  try {
    const readSnapshot = await repository.emailProvider.getEmailProvider(
      firebaseAdmin
    )
    const data = readSnapshot.docs.map((providerData) => ({
      id: providerData.id,
      ...providerData.data(),
    }))
    return res.status(200).send({ data })
  } catch (error) {
    return res.status(400).send(error)
  }
}

export const providerUpdateService = (firebaseAdmin: admin.app.App) => async (
  req: express.Request,
  res: express.Response
) => {
  try {
    await repository.emailProvider.updateEmailProvider(firebaseAdmin, req.body)
    return res.status(200).send({ data: req.body })
  } catch (error) {
    return res.status(400).send(error)
  }
}
