import * as controller from "./controller";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
const firebaseAdmin = admin.initializeApp();

exports.user = functions.https.onRequest(controller.user(firebaseAdmin));
exports.email = functions.https.onRequest(controller.email(firebaseAdmin));
